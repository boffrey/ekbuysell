﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace EkBuySell
{
    class Program
    {
        private static dynamic Server;
        private static bool Debug;

        static void Main(string[] args)
        {
            Console.WriteLine("EkBuySell v0.3");
            Console.WriteLine("https://bitbucket.org/boffrey/ekbuysell");
            if (args.Length < 3)
            {
                Console.WriteLine("");
                Console.WriteLine("Will purchase gold packs and instantly sell the purchased cards.");
                Console.WriteLine("");
                Console.WriteLine("Usage: ");
                Console.WriteLine("EkBuySell <username> <password> <number> [-debug]");
                Console.WriteLine("");
                Console.WriteLine("username - your Elemental Kingdoms username");
                Console.WriteLine("password - your Elemental Kingdoms password");
                Console.WriteLine("number   - number of cards to buy and sell (must be multiple of 50)");
                Console.WriteLine("-debug   - Writes additional debug info to debug.txt");
                return;
            }

            var username = args[0];
            var password = args[1];
            var count = int.Parse(args[2]);
            count = count - (count % 50);
            Debug = args.Any(a => a == "-debug");

            Console.WriteLine("");
            Console.WriteLine("Buying and selling " + count + " cards...");
            Console.WriteLine("");

            var cookie = Login(username, password);
            BuyAndSell(cookie, count);
        }

        public static CookieContainer Login(string username, string password)
        {
            var logincookie = new CookieContainer();

            DebugOutput("=============================================================================");

            var formbody = Http("get", "http://mobile.arcgames.com/user/login/?gameid=51", logincookie);

            DebugOutput(formbody);

            var emailInputPrompt = formbody.Substring(formbody.IndexOf("type=\"email\" id=\"un\"") + 27);
            emailInputPrompt = emailInputPrompt.Substring(0, emailInputPrompt.IndexOf('"'));

            DebugOutput("email input=" + emailInputPrompt);

            var pwInputPrompt = formbody.Substring(formbody.IndexOf("type=\"password\" id=\"pw\"") + 30);
            pwInputPrompt = pwInputPrompt.Substring(0, pwInputPrompt.IndexOf('"'));

            DebugOutput("password input=" + pwInputPrompt);

            var captchaPrompt = formbody.Substring(formbody.IndexOf("id=\"captcha_login\"") + 25);
            captchaPrompt = captchaPrompt.Substring(0, captchaPrompt.IndexOf('"'));

            DebugOutput("captcha input=" + captchaPrompt);

            dynamic login = HttpJson("post",
                "http://mobile.arcgames.com/user/login/?gameid=51",
                logincookie,
                new[]
                {
                    new KeyValuePair<string, string>(emailInputPrompt, username),
                    new KeyValuePair<string, string>(pwInputPrompt, password),
                    new KeyValuePair<string, string>(captchaPrompt, ""),
                });
            var outstr = login.ToString();
            DebugOutput("LOGIN RESPONSE:");
            DebugOutput(outstr);
            
            // Failed to login - show message
            if(!login.result.Value)
            {
                Console.WriteLine(login.msg);
                Environment.Exit(0);
            }

            var nick = login.loginstatus.Value;
            nick = nick.Substring(nick.IndexOf(":") + 1);
            nick = nick.Substring(0, nick.IndexOf(":"));

            // Get user/server details
            var plogincookie = new CookieContainer();
            dynamic plogin = HttpJson("post",
                "http://master.ek.ifreeteam.com/mpassport.php?do=plogin",
                plogincookie,
                new[]
                {
                    new KeyValuePair<string, string>("plat", "pwe"),
                    new KeyValuePair<string, string>("uin", nick),
                    new KeyValuePair<string, string>("nickName", nick),
                    new KeyValuePair<string, string>("userType", "2")
                });
            DebugOutput("PLOGIN RESPONSE:");
            DebugOutput(plogin.ToString());

            Server = plogin.data.current.GS_IP.Value;

            // login to correct server
            var mplogin = HttpJson("post",
                Server + "login.php?do=mpLogin",
                plogincookie,
                new[]
                {
                    new KeyValuePair<string, string>("plat", "pwe"),
                    new KeyValuePair<string, string>("uin", (string)plogin.data.uinfo.uin),
                    new KeyValuePair<string, string>("nickName", (string)plogin.data.uinfo.nick),
                    new KeyValuePair<string, string>("Devicetoken", "51bc601bc-4ab4-45d3-8110-c374ffee3d0d"),
                    new KeyValuePair<string, string>("userType", "2"),
                    new KeyValuePair<string, string>("MUid", (string)plogin.data.uinfo.MUid),
                    new KeyValuePair<string, string>("ppsign", (string)plogin.data.uinfo.ppsign),
                    new KeyValuePair<string, string>("sign", (string)plogin.data.uinfo.sign),
                    new KeyValuePair<string, string>("nick", (string)plogin.data.uinfo.nick),
                    new KeyValuePair<string, string>("time", (string)plogin.data.uinfo.time),
                    new KeyValuePair<string, string>("Udid", "00-C0-7B-9F-0A-01"),
                    new KeyValuePair<string, string>("Origin", "IOS_PW")
                });
            DebugOutput("MPLOGIN RESPONSE:");
            DebugOutput(mplogin.ToString());

            return plogincookie;
        }

        private static void DebugOutput(string text)
        {
            if (!Debug) return;

            using (var file = File.AppendText("debug.txt"))
            {
                file.WriteLine(text);
            }
        }

        public static void BuyAndSell(CookieContainer cookie, int count)
        {
            var requestedCards = count;

            while(count >= 50)
            {
                var cardsBought = 0;
                var cardTypesList = new List<int>();
                while(count >=50 && cardsBought < 1000 && cardsBought < count)
                {
                    cardsBought += 50;
                    Console.Write("Buying " + cardsBought + " cards\r");
                    var productType = new List<KeyValuePair<string, string>>();
                    productType.Add(new KeyValuePair<string, string>("version", "new"));
                    productType.Add(new KeyValuePair<string, string>("GoodsId", "8"));
                    try
                    {

                        // send the buy request
                        var buyResult = HttpJson("post",
                            Server + "shopnew.php?do=Buy&v=1170&phpp=ANDROID_ARC&phpl=EN&pvc=1.7.4&phps=770908883&phpk=0",
                            cookie,
                            productType);

                        if (buyResult.status.Value != 1)
                        {
                            Console.WriteLine("Failed to buy cards... Exiting!");
                            Environment.Exit(0);
                        }

                        // build up a list of all card types
                        var newCardTypes = ((string)buyResult.data.CardIds.Value).Split('_').Select(int.Parse).ToList();
                        cardTypesList.AddRange(newCardTypes);
                    }
                    catch (JsonReaderException)
                    {
                        Console.WriteLine("Failed... Retrying.");
                        cardsBought -= 50;
                        continue;
                    }
                }

                // Get ids of all bought cards
                dynamic cards = HttpJson("get",
                    Server + "card.php?do=GetUserCards&v=1170&phpp=ANDROID_ARC&phpl=EN&pvc=1.7.4&phps=770908883&phpk=0",
                    cookie);
                
                // Build selling list, by looking up most recent cards 
                // corresponding to each of the buoght card types.
                var cardListStr = new StringBuilder();
                var cardList = cards.data.Cards as JArray;
                var boughtCardTypePointer = cardTypesList.Count - 1;
                for(var i=cardList.Count-1; i>=0 && boughtCardTypePointer>=0; i--)
                {
                    dynamic thisCard = cardList[i] as JObject;
                    var expectedCardType = cardTypesList[boughtCardTypePointer];

                    if (thisCard.CardId.Value != expectedCardType) continue;

                    if (cardListStr.Length > 0) cardListStr.Append("_");
                    cardListStr.Append(thisCard.UserCardId);
                    boughtCardTypePointer--;
                }

                var totalCardsBought = cardListStr.ToString().Split('_').Length;

                Console.WriteLine();
                for (var offset = 0; offset < totalCardsBought; offset += 50)
                {
                    var payload = new List<KeyValuePair<string, string>>();
                    var subCards = string.Join("_",cardListStr.ToString().Split('_').Skip(offset).Take(50).ToArray());
                    payload.Add(new KeyValuePair<string, string>("Cards", subCards));
                    payload.Add(new KeyValuePair<string, string>("Runes", ""));

                    Console.Write("Selling " +(offset+50)+" cards\r");
                    var failed = true;
                    while (failed)
                    {
                        try
                        {
                            // "_sid=4ov0nte1q65h5fgseasnm7hk73; expires=Tue, 03-Mar-2015 14:22:11 GMT; path=/"
                            var sellResult = HttpJson("post",
                                Server + "card.php?do=SaleCardRunes&v=1170&phpp=ANDROID_ARC&phpl=EN&pvc=1.7.4&phps=770908883&phpk=0",
                                cookie,
                                payload);

                            if (sellResult.status.Value != 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Failed to sell cards... Exiting!");
                                Environment.Exit(0);
                            }

                            failed = false;
                        }
                        catch (JsonReaderException)
                        {
                            Console.WriteLine();
                            Console.WriteLine("Failed... Retrying.");
                        }
                    }
                }
                count -= cardsBought;
                Console.WriteLine();
                Console.WriteLine("Bought and sold " + (requestedCards - count) + " of " + requestedCards + " cards\r");
            }

            Console.WriteLine("Done!");
        }

        private static JObject HttpJson(string method, string url, CookieContainer cookieJar =null, IEnumerable<KeyValuePair<string, string>> contentString = null)
        {
            string result;

            try
            {
                result = Http(method, url, cookieJar, contentString);
            }
            catch (Exception)
            {
                DebugOutput("EXCEPTION MAKING REQUEST:");
                DebugOutput("method=" + method);
                DebugOutput("url=" + url);
                if(cookieJar!=null)
                {
                    DebugOutput("cookies:");
                    foreach (var c in cookieJar.GetCookies(new Uri(url)))
                    {
                        var cookie = (Cookie)c;
                        DebugOutput(cookie.Name + "=" + cookie.Value);
                    }
                }
                if (contentString != null)
                {
                    DebugOutput("args:");
                    foreach (var c in contentString)
                    {
                        DebugOutput(c.Key + "=" + c.Value);
                    }
                }
                throw;
            }
            
            try
            {
                return JObject.Parse(result);
            }
            catch (Exception)
            {
                DebugOutput("EXCEPTION PARSING JSON:");
                DebugOutput(result);
                throw;
            }
        } 

        private static string Http(string method, string url, CookieContainer cookieJar, IEnumerable<KeyValuePair<string, string>> contentString = null)
        {
            if (cookieJar == null)
            {
                cookieJar = new CookieContainer();
            }
            var handler = new HttpClientHandler
            {
                CookieContainer = cookieJar,
                UseCookies = true,
                UseDefaultCredentials = false,
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            
            var client = new HttpClient(handler);
            
            client.DefaultRequestHeaders.Add("X-Unity-Version", "4.5.4f1");
            //client.DefaultRequestHeaders.Add("Cookie", cookie);
            client.DefaultRequestHeaders.Add("User-Agent",
                "Dalvik/1.6.0 (Linux; U; Android 4.2.2; GT-P5200 Build/JDQ39E)");
            client.DefaultRequestHeaders.Add("Connection", "Keep-Alive");
            client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip");

            try
            {
                if (method.ToLower() == "get")
                {
                    var getbytes = client.GetAsync(url).Result.Content.ReadAsByteArrayAsync().Result;
                    var getresponseContent = Encoding.UTF8.GetString(getbytes, 0, getbytes.Length);
                    return getresponseContent;
                }

                var requestContent = new FormUrlEncodedContent(contentString);

                var bytes = client.PostAsync(url, requestContent).Result.Content.ReadAsByteArrayAsync().Result;

                var responseContent = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                return responseContent;
            }
            catch (Exception)
            {
                Console.WriteLine("An error occurred trying to call " + url + " - app will exit.");
                Environment.Exit(-1);
                return null;
            }
        }
    }
}
