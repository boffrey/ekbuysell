EkBuySell - A tool to automatically buy and then sell gold packs in Elemental Kingdoms

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE 
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE 
FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


Will purchase gold packs and instantly sell the purchased cards.

Usage:
EkBuySell <username> <password> <number> [-debug]

username - your Elemental Kingdoms username
password - your Elemental Kingdoms password
number   - number of cards to buy and sell (must be multiple of 50)
-debug   - optionally write debug output to debug.txt

WARNING
=======

While, as-of v0.2, EkBuySell only sells cards of the type that it buys, it might still be possible
for other automated tools (such as EK Unleashed, or EK Utils) to cause unexpected behaviour. 
EkBuySell will look at the most recent cards and sell them only if they match the types of the 
purchased cards. If you are at all unsure, do not run other automation tools at the same time.


More Information
================

Extract the files from the zip.
Open a windows command prompt (Windows+R, cmd <return>)
Change to the directory where you unzipped the files (cd c:\windows\desktop\EkBuySell)
Type in EkBuySell "<username>" "password" number
where:
	username is the login username (usually your email address)
	password is your account password
	number is the number of cards to buy and sell
	
Enclose the username and password in quotes (") to ensure they are passed to the program correctly.


TODO
====

Add option to select which types of cards to sell (1, 2, 3 stars)
Add option to only buy/only sell.


History
=======

v0.2
Debug switch to output useful stuff to debug.txt
More error messages
Now checks if login is successful
Now checks if purchase is successful
Now checks if sale is successful
Checks that type of cards match the purchased card types before selling
(won't be possible to accidentally sell 5* cards any more, even if running along side EKUnleashed!) 

v0.1
Initial Release
Basic functionality 