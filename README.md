# EkBuySell #

A tool to automatically buy and then sell gold packs in Elemental Kingdoms.

### Downloads ###

* [Latest Version (0.3)](https://bitbucket.org/boffrey/ekbuysell/downloads/EkBuySell-0.3.zip)

### Usage ###

EkBuySell <username> <password> <number> [-debug]

* username - your Elemental Kingdoms username
* password - your Elemental Kingdoms password
* number   - number of cards to buy and sell (must be multiple of 50)
* -debug   - optionally write debug output to debug.txt


### Quick Start ###

* Extract the files from the zip.
* Open a windows command prompt (Windows+R, cmd <return>)
* Change to the directory where you unzipped the files (cd c:\windows\desktop\EkBuySell)
* Type in EkBuySell "<username>" "password" number
where:
	username is the login username (usually your email address)
	password is your account password
	number is the number of cards to buy and sell
	
Enclose the username and password in quotes (") to ensure they are passed to the program correctly.

### To-Do ###

Add option to select which types of cards to sell (1, 2, 3 stars)
Add option to only buy/only sell.

### History ###

v0.2
* Debug switch to output useful stuff to debug.txt
* More error messages
* Now checks if login is successful
* Now checks if purchase is successful
* Now checks if sale is successful
* Checks that type of cards match the purchased card types before selling (won't be possible to accidentally sell 5* cards any more, even if running along side EKUnleashed!) 

v0.1
* Initial Release
* Basic functionality